# www to non-www redirect -- duplicate content is BAD:
# choose between www and non-www, listen on the *wrong* one and redirect to
# the right one -- http://nginx.org/Pitfalls#Server_Name
server {
    # Don't forget to tell on which port this server listens
    listen [::]:80;
    listen 80;

    # Listen on the www host
    server_name     www.example.com;

    # And redirect to the non-www host (declared below)
    return 301 $scheme://example.com$request_uri;
}

server {
    # listen [::]:80 accept_filter=httpready; # for FreeBSD
    # listen 80 accept_filter=httpready; # for FreeBSD
    # listen [::]:80 deferred; # for Linux
    # listen 80 deferred; # for Linux
    listen [::]:80;
    listen 80;

    # The host name to respond to
    server_name example.com;

    # Path for static files
    root    /sites/example.com/public;

    # Specify a charset
    charset utf-8;

    # Custom 404 page
    error_page 404 /404.html;

    # Include the basic config set;
    include basic.conf;
}
